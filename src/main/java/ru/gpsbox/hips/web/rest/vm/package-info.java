/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.gpsbox.hips.web.rest.vm;
