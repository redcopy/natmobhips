package ru.gpsbox.hips.service.impl;

import ru.gpsbox.hips.service.TagService;
import ru.gpsbox.hips.domain.Tag;
import ru.gpsbox.hips.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Tag.
 */
@Service
public class TagServiceImpl implements TagService{

    private final Logger log = LoggerFactory.getLogger(TagServiceImpl.class);
    
    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    /**
     * Save a tag.
     *
     * @param tag the entity to save
     * @return the persisted entity
     */
    @Override
    public Tag save(Tag tag) {
        log.debug("Request to save Tag : {}", tag);
        Tag result = tagRepository.save(tag);
        return result;
    }

    /**
     *  Get all the tags.
     *  
     *  @return the list of entities
     */
    @Override
    public List<Tag> findAll() {
        log.debug("Request to get all Tags");
        List<Tag> result = tagRepository.findAll();

        return result;
    }

    /**
     *  Get one tag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public Tag findOne(String id) {
        log.debug("Request to get Tag : {}", id);
        Tag tag = tagRepository.findOne(id);
        return tag;
    }

    /**
     *  Delete the  tag by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Tag : {}", id);
        tagRepository.delete(id);
    }
}
