package ru.gpsbox.hips.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import ru.gpsbox.hips.service.BlogService;
import ru.gpsbox.hips.domain.Blog;
import ru.gpsbox.hips.repository.BlogRepository;
import ru.gpsbox.hips.service.dto.BlogDTO;
import ru.gpsbox.hips.service.mapper.BlogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Blog.
 */
@Service
public class BlogServiceImpl implements BlogService{

    private final Logger log = LoggerFactory.getLogger(BlogServiceImpl.class);

    private  BlogRepository blogRepository;

    private  BlogMapper blogMapper;

    public BlogServiceImpl(BlogRepository blogRepository, BlogMapper blogMapper) {
        this.blogRepository = blogRepository;
        this.blogMapper = blogMapper;
    }


    /**
     * Save a blog.
     *
     * @param blogDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BlogDTO save(BlogDTO blogDTO) {
        log.debug("Request to save Blog : {}", blogDTO);
        Blog blog = blogMapper.blogDTOToBlog(blogDTO);
        blog = blogRepository.save(blog);
        BlogDTO result = blogMapper.blogToBlogDTO(blog);
        return result;
    }

    /**
     *  Get all the blogs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<BlogDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Blogs");
        Page<Blog> result = blogRepository.findAll(pageable);
        return result.map(blog -> blogMapper.blogToBlogDTO(blog));
    }

    /**
     *  Get one blog by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public BlogDTO findOne(String id) {
        log.debug("Request to get Blog : {}", id);
        Blog blog = blogRepository.findOne(id);
        BlogDTO blogDTO = blogMapper.blogToBlogDTO(blog);
        return blogDTO;
    }

    /**
     *  Delete the  blog by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Blog : {}", id);
        blogRepository.delete(id);
    }
}
