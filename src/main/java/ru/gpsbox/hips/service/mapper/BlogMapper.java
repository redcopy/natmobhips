package ru.gpsbox.hips.service.mapper;

import ru.gpsbox.hips.domain.*;
import ru.gpsbox.hips.service.dto.BlogDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Blog and its DTO BlogDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BlogMapper {

    BlogDTO blogToBlogDTO(Blog blog);

    List<BlogDTO> blogsToBlogDTOs(List<Blog> blogs);

    Blog blogDTOToBlog(BlogDTO blogDTO);

    List<Blog> blogDTOsToBlogs(List<BlogDTO> blogDTOs);
}
