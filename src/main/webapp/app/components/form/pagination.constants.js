(function() {
    'use strict';

    angular
        .module('natmobApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
